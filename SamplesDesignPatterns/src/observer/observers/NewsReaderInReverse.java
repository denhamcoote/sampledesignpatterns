package observer.observers;

import observer.NewsItem;
import observer.Observer;

public class NewsReaderInReverse extends NewsReader implements Observer {
	
	
	public NewsReaderInReverse(String observerName) {
		super(observerName);
	}

	@Override
	public void displayNews(NewsItem newsItem) {
		System.out.println("Observer: " + observerName);
		System.out.println(new StringBuilder(newsItem.getHeadline()).reverse().toString());
		System.out.println(new StringBuilder(newsItem.getBody()).reverse().toString());
		System.out.println(new StringBuilder(newsItem.getDate().toString()).reverse().toString());
		System.out.println("------------------------");
	}

}
