package observer.observers;

import observer.NewsItem;
import observer.Observer;

public class NewsReaderOnlyHeadlines extends NewsReader implements Observer {
	
	
	public NewsReaderOnlyHeadlines(String observerName) {
		super(observerName);
	}

	@Override
	public void displayNews(NewsItem newsItem) {
		System.out.println("Observer: " + observerName);
		System.out.println(newsItem.getHeadline());
		System.out.println("------------------------");
	}

}
