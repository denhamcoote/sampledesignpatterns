package observer.observers;

import observer.NewsItem;
import observer.Observer;

public class NewsReader implements Observer {
	
	String observerName;
	NewsItem localCopyOfNewsItem;

	public NewsReader(String observerName) {
		this.observerName = observerName;
	}

	public String getObserverName() {
		return observerName;
	}

	public void setObserverName(String observerName) {
		this.observerName = observerName;
	}

	public void update(Object object) {
		// Cast the object to right type
		localCopyOfNewsItem = (NewsItem)object;
		displayNews(localCopyOfNewsItem);
	}
	
	public void displayNews(NewsItem newsItem) {
		System.out.println("Observer: " + observerName);
		System.out.println(newsItem.getHeadline());
		System.out.println(newsItem.getBody());
		System.out.println(newsItem.getDate());
		System.out.println("------------------------");
	}

}
