package observer;

import java.util.ArrayList;
import java.util.List;

public class NewsCaster implements Subject {
	
	private List<Observer> observers = new ArrayList<Observer>();
	private NewsItem newsItem;

	public void register(Observer o) {
		observers.add(o);
	}

	public void remove(Observer o) {
		int i = observers.indexOf(o);
		if (i >= 0){
			observers.remove(i);
		}
	}

	public void notifyObservers() {
		for (Observer o : observers) {
			o.update(newsItem);
		}
	}
	
	// We could just call notifyObservers (which the interface specifies)
	// but having this lets us use a different name for the method in the calling class
	 // and we can do logging or other stuff
	public void broadcast() {
		System.out.println("=== ABOUT TO BROADCAST ===");
		notifyObservers();
	}

	public NewsItem getNewsItem() {
		return newsItem;
	}

	public void setNewsItem(NewsItem newsItem) {
		this.newsItem = newsItem;
	}

}
