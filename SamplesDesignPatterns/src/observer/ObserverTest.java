package observer;

import java.util.Date;

import observer.observers.NewsReader;
import observer.observers.NewsReaderInReverse;
import observer.observers.NewsReaderOnlyHeadlines;

public class ObserverTest {

	public static void main(String[] args) throws InterruptedException {
		
		// Create some observers
		// Added some different implementations of the observers. Nothing else
		// changes and as far as this class is concerned they're all treated the
		// same (ie, they're all of type Observer)
		Observer o1 = new NewsReader("Observer 1");
		Observer o2 = new NewsReaderInReverse("Observer 2");
		Observer o3 = new NewsReaderOnlyHeadlines("Observer 3");

		// Create the thing that pushes out the content
		NewsCaster nc = new NewsCaster();
		
		// Create the content that is being pushed out
		NewsItem newsItem = new NewsItem();
		newsItem.setHeadline("First headline");
		newsItem.setBody("This is the body of the first news item");
		newsItem.setDate(new Date());
		
		nc.setNewsItem(newsItem);
			
		// Register the observers with the observable
		nc.register(o1);
		nc.register(o2);
		
		// Send out the first update
		nc.broadcast();
		
		// Let some time pass before the next broadcast (2 seconds)
		System.out.println("\n=== Some time passes ===\n");
		Thread.sleep(2000);
		
		// Change the content
		newsItem.setHeadline("Second headline");
		newsItem.setBody("This is the body of the second news item");
		newsItem.setDate(new Date());

		// Add a 3rd observer
		nc.register(o3);
		
		// Send out the second update
		nc.broadcast();
		
		// Let some time pass before the next broadcast (2 seconds)
		System.out.println("\n=== Some time passes ===\n");
		Thread.sleep(2000);
		
		// Change the content
		newsItem.setHeadline("Third headline");
		newsItem.setBody("This is the body of the third news item");
		newsItem.setDate(new Date());

		// Remove the 2nd observer
		nc.remove(o2);
		
		// Send out the third update
		nc.broadcast();
		
	}

}
